#include <iostream>

using namespace std;

struct list {
    int foo;
    int bar;
    list* next;
};

void countToTen() {
    cout << "Lets count to 10" << endl;
    for (int i = 0; i < 10; i++) {
        cout << i << endl;
    }
}

int* addToNumOnHeap(int* num) {
    *num += 5;
    return num;
}

void addfoos (list* start) {
    while (start -> next != NULL){
        start -> foo += start -> bar;
    }
}

void bug1();
void bug2();
void bug3();
void bug4();

int main() {
    bug1();
    bug2();
    bug3();
    //bug4();
}


list* makeLL(int length) {
    list* head = new list;
    list* current = head;
    list* node;
    head -> foo = 3;
    head -> bar = 5;
    for(int i = 0; i < length; i++) {
        node = new list;
        node -> foo = i;
        node -> bar = 2 * i;
        current -> next = node;
        current = node;
        if (i == length - 1) {
            current -> next = NULL;
        }
    }
    return head;
}

void printLL(list* head) {
    while (head -> next != NULL) {
        cout << "Foo: " << head -> foo << "     Bar: " << head -> bar << endl;
        head = head -> next;
    }
}

char** getStr() {
    char* h = "hello world\n";
    return &h;
}

void bug1() {
    cout << endl << ">>>>>>>>>>>>>>>>>>>> TEST 1 <<<<<<<<<<<<<<<<<<<<<" << endl;
    countToTen();
}


void bug2() {
    cout << endl << ">>>>>>>>>>>>>>>>>>>> TEST 2 <<<<<<<<<<<<<<<<<<<<<" << endl;
    i valgrind --leak-check=yes
    *test = 0;
    test = addToNumOnHeap(test);
    cout << "Expected 5, got: " << *test << endl;
}


void bug3() {
    cout << endl << ">>>>>>>>>>>>>>>>>>>> TEST 3 <<<<<<<<<<<<<<<<<<<<<" << endl;
    list* linkL = makeLL(5);
    printLL(linkL);
    cout << endl << "Adding Bar to Foo" << endl;
    //addfoos(linkL);
    printLL(linkL);
    delete linkL;
    return;
}


void bug4() {
    cout << endl << ">>>>>>>>>>>>>>>>>>>> TEST 4 <<<<<<<<<<<<<<<<<<<<<" << endl;
    char** hello;
    hello = getStr();
    cout << *hello;
    return;
}
